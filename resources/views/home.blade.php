@extends('layouts.app')
@section('title', 'KB')
@section('content')
<!--header start here-->
 <div class="header" id="home" style="background-image:url({{url($slider->image)}})">
       <div class="container">
		  	   <div class="header-main">
		  	    	   <div class="logo">
		  	                        <a href="index.html"><img src="{{url('images/logo-b.png')}}"></a>
		  	           </div>
		  	           <span class="menu"><img src="{{url('images/icon.png')}}"> </span> 
		  	           <div class="clear"> </div>
		  	           <div class="header-right">  	         	
		 	          	            <ul class="res">
		  	             	         	<li><a class="active" href="/">Home</a></li>
		  	             	         	
		  	             	         	<li><a href="#">Contact</a></li>
		  	             	         </ul> 
		  	             	         
		                                                                   
		  	          </div>
		  	      <div class="clearfix"> </div>
		  	  </div>
		  	          	     <h1>{!! $slider->title !!}</h1>
                             <span class="a"> </span>
	   </div>    
 </div>
<!--work start here-->
<div class="work">
	<div class="container">
		<div class="row work-row">
		@foreach($products as $product)
			<a href="#small-dialog{{$product->id}}" class="thickbox play-icon popup-with-zoom-anim"><div class="col-md-4 work-column">
				<img src="{{url($product->image)}}" alt="" class="img-responsive img-cap">
				<div class="caption">
					  <div class="text">
					        <h3>{{$product->title}}</h3>
					  </div>
				</div>
			</div>
			</a>
			<div id="small-dialog{{$product->id}}" class="mfp-hide">
				<div class="image-top">
					<img src="{{url($product->image)}}"/>
				</div>
			</div>
		@endforeach
			<div class="clearfix"> </div> 
		</div>
		
		
	</div>
</div>
@endsection
