<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') - Kavita Boutique</title>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<link href="{{url('css/style.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!--web-fonts-->
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
<link href="{{url('css/popuo-box.css')}}" rel="stylesheet" type="text/css" media="all"/>
<!--js-->

<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="{{url('css/magnific-popup.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
        @yield('content')
        <div class="contact">
 	   <div class="container">
 	   	         <div class="contact-main">
 	   	         	               <h3>Get in touch</h3>
 	   	         	              <div class="row contact-top">
 	   	         	              	       <div class="col-md-4 contact-left">
 	   	         	              	       	           <h3> Office</h3>
 	   	         	              	       	           <p> KavitaButique</p>
 	   	         	              	       	           <p> Sector 30-A</p>
 	   	         	              	       	           <p> Chandigarh</p>
 	   	         	              	       	           <p>email : <a href="mailto:kavitaboutique00@gmail.com" >kavitaboutique00@gmail.com</a></p>
 	   	         	              	       </div>
 	   	         	              	       <div class="col-md-4 contact-left">
 	   	         	              	       	          <h4>Social</h4>
 	   	         	              	       	       <ul> 
 	   	         	              	       	           <li><a href="#"> Facebook</a></li>
 	   	         	              	       	           <li><a href="#">Twitter </a></li>
 	   	         	              	       	           <li><a href="#">Linkedin</a></li>
 	   	         	              	       	           <li><a href="#"> Behance</a></li>
 	   	         	              	       	      </ul>
 	   	         	              	       </div>
 	   	         	              	        <div class="col-md-4 contact-left">
 	   	         	              	       	          <h5>SAY HELLO TO US</h5>
 	   	         	              	       	         <form>
 	   	         	              	       	         	  <input type="text" class="textbox" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}"/><br>
 	   	         	              	       	         	  <textarea value="Message"onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'message';}"/> Message </textarea><br>
 	   	         	              	       	         	  <input type="submit" value="submit">
 	   	         	              	       	         </form>
 	   	         	              	       </div>
 	   	         	              </div>
 	   	         </div>
 	   </div>
 </div>
 <!--footer start here-->
 <div class="footer">
 	       <div class="container">
 	           	 <div class="footer-main">
 	           	 	       <p> 2017 &copy  <a href="#">Kavita Boutique </a></p>
 	           	 </div> 
 	       </div>
 	      
					<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
 </div>
 <!---pop-up-box---->
 <script src="{{url('js/jquery.min.js')}}"></script>
 <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{url('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{url('js/easing.js')}}"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
<!-- //end-smoth-scrolling -->
<script src="{{url('js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
 <script type="text/javascript" src="{{url('js/modernizr.custom.min.js')}}"></script>    
 
					<script src="{{url('js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
			<!---//pop-up-box---->
            <script>
			$(document).ready(function() {
				$('.popup-with-zoom-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
			});
		});
        </script>
         <script>
			                                                      $( "span.menu").click(function() {
			                                                                                        $(  "ul.res" ).slideToggle("slow", function() {
			                                                                                         // Animation complete.
			                                                                                         });
			                                                                                         });
		                                                     </script>
	</body>
</html>