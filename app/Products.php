<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    public static function get()
    {
        return Products::where('user_id', \Auth::user()->id)->where('deleted_at', NULL)->get();       
    }
}
