<?php

namespace App\Http\Controllers;

use App\Products, App\User;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class APIController extends Controller
{
    protected $user;
    
        function __construct(){
            try{
                $this->user= JWTAuth::parseToken()->authenticate();
            }
            catch(\Exception$e){}
        }
    public function login(Request $request, User $post)
    {
        if(!$request['email'])
            return api()->notValid(['errorMsg'=>'Email is required']);
        if(!$request['password'])
            return api()->notValid(['errorMsg'=>'Password is required']);
        return $post->post($request);
    }
    
    public function getProducts()
    {
        $get = Products::get();
        if($get->isEmpty())
            return api()->notFound(['errorMsg'=>'Product not available']);
        return api(['data'=>$get]);
    }

    public function postProducts()
    {
        $input = \Request::all();
        $errors = [
            'title'=>'Product Title',
            'image'=>'Product Image'
        ];
        $fields = [
            'title'=>'required|regex:/^[A-Za-z][A-Za-z0-9- ]+/|max:40|min:3'
        ];
        $uservalid=\Validator::make($input,$fields,$errors);
        
        $uservalid->setAttributeNames($errors);

        if($uservalid->fails())
        {
            
            return \api::notValid(['errorMsg'=>$uservalid->errors()->first()]);
        }
        else
        {
            if(!isset($input['image']))
            {
                return api()->notValid(['errorMsg'=>'Product Image required']);
            }
            else{
                $files = \Request::file('image');
                $uploadpath = 'upload/products';
                $ex=$files->getClientOriginalExtension();
                    $filename=rand(11111,99999).".".$ex;
                    $uploadSuccess=$files->move($uploadpath,$filename);
                    $img = \Image::make($uploadpath.'/'.$filename)->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                        })->save($uploadpath.'/'.$filename);
                $insert = \DB::table('products')->insert([
                    'title'=>$input['title'],
                    'image'=>$uploadpath.'/'.$filename,
                    'user_id'=>$this->user->id
                ]);

                if($insert)
                    return api(['success'=>'Success to submitted product']);
                return api()->notValid(['errorMsg'=>'could not submitted product']);
            }
        }
    }

    public function getBanner()
    {
        $get = \DB::table('banner')->orderBy('id','DESC')->first();
        if($get)
            return api(['data'=>$get]);
        return api()->notFound(['errorMsg'=>'not enough banners']);
    }

    public function postBanner()
    {
        $title = \Request::get('title');
        if(!$title)
        {
            return api()->notValid(['errorMsg'=>'title is required']);
        }
        $files = \Request::file('banner');
        if(!$files)
        {
            return api()->notValid(['errorMsg'=>'Banner Image required']);
        }
        else
        {
            
            $uploadpath = 'upload/banner';
            $ex=$files->getClientOriginalExtension();
                $filename=rand(11111,99999).".".$ex;
                $uploadSuccess=$files->move($uploadpath,$filename);
                $img = \Image::make($uploadpath.'/'.$filename)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                    })->save($uploadpath.'/'.$filename);
            
                $update =  \DB::table('banner')->update([
                    'image'=>$uploadpath.'/'.$filename,
                    'title'=>$title
                ]);
                if(!$update)
                {
                 $insert =  \DB::table('banner')->insert([
                        'image'=>$uploadpath.'/'.$filename,
                        'title'=>$title
                    ]);
                    if($insert)
                            return api(['success'=>'Success to submitted product']);
                    return api()->notValid(['errorMsg'=>'could not submitted product']);
                 }
                 else if($update)
                    return api(['success'=>'Success to submitted product']);
            return api()->notValid(['errorMsg'=>'could not submitted product']);
        }
    }
}
